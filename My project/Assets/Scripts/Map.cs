using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class Map
{
    public string mapName;      // Nombre del mapa.
    public int mapWidth;        // Ancho del mapa.
    public int mapHeight;       // Alto del mapa.

    public int[] mapLayout;     // Layout del mapa. JsonUtility no soporta estructuras multidimensionales (array 2D, etc.),
                                // por lo que hay que transformarlo en un array 1D.
    

    public Map(int[] m, int w, int h, string n)
    {
        mapName = n;
        mapWidth = w;
        mapHeight = h;
        mapLayout = m;
    }

    public static int[,] From1DTo2D(Map baseMap)
    {
        int[,] map2D = new int[baseMap.mapWidth, baseMap.mapHeight];

        for (int x = 0; x < baseMap.mapWidth; x++)
        {
            for (int y = 0; y < baseMap.mapHeight; y++)
            {
                map2D[x, y] = baseMap.mapLayout[y * baseMap.mapWidth + x];
            }
        }

        return map2D;
    }

    public static int[] From2DTo1D(int [,] baseMap, int mapWidth, int mapHeight)
    {
        int[] map1D = new int[mapWidth * mapHeight];

        int i = 0;

        for (int y = 0; y < mapHeight; y++)
        {
            for(int x = 0; x < mapWidth; x++)
            {
                map1D[i++] = baseMap[x, y];
            }
        }

        return map1D;
    }

}
