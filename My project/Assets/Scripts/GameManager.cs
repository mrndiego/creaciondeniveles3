using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public Map[] mapList;   // Array de mapas. Contiene todos los niveles.
    int mapIndex = 0;       // Indice del nivel actual.

    // 1 = Pared, 2 = Bloque movil, 3 = Target, 4 = Player. Cualquier otro numero se considera vacio.
    // Prefabs de los bloques.
    public GameObject WallPrefab;
    public GameObject BlockPrefab;
    public GameObject TargetPrefab;
    public GameObject PlayerPrefab;

    // GameObjects que almacenan los "tiles" instanciados. Cada uno almacena un tipo correspondiente.
    public GameObject WallParent;
    public GameObject BlockParent;
    public GameObject TargetParent;

    void Start()
    {
        LoadMaps();         // Cargar todos los mapas.
        DrawCurrentMap();   // Dibujar el mapa actual.
    }

    void LoadMaps()
    {
        // Cargar todos los mapas que haya en la carpeta Assets/Resources/Maps.
        TextAsset[] rawMaps = Resources.LoadAll<TextAsset>("Maps");    // Primero se cargan los mapas como TextAsset, texto sin formato.
        mapList = new Map[rawMaps.Length];                             // Se inicializa mapList con el mismo tama�o que rawMaps.

        for(int i = 0; i < rawMaps.Length; i++)
        {
            mapList[i] = JsonUtility.FromJson<Map>(rawMaps[i].text);    // Se cargan todos y cada uno de los mapas en mapList.
        }
    }



    void DrawCurrentMap()
    {
        int[,] map = Map.From1DTo2D(mapList[mapIndex]); // Transformar la representacion del mapa actual a un array 2D.

        // Dibujar el mapa. Esto incluye colocar las paredes, bloques moviles, targets y al jugador.
        // Se recorre el array map y se instancian los objetos correspondientes en sus posiciones asignadas.
        for (int i = 0; i < map.GetLength(0); i++)
        {
            for (int j = 0; j < map.GetLength(1); j++)
            {
                // Debido a la forma en que Unity interpreta las coordenadas, para representar el mapa de la misma forma que lo creamos en
                // el array debemos colocar cada elemento en las coordenadas (i,-j).
                switch (map[i, j])
                {
                    case 1:
                        Instantiate(WallPrefab, new(i, -j, 0), Quaternion.identity, WallParent.transform);
                        break;
                    case 2:
                        Instantiate(BlockPrefab, new(i, -j, 0), Quaternion.identity, BlockParent.transform);
                        break;
                    case 3:
                        Instantiate(TargetPrefab, new(i, -j, 0), Quaternion.identity, TargetParent.transform);
                        break;
                    case 4:
                        Instantiate(PlayerPrefab, new(i, -j, 0), Quaternion.identity, transform);
                        break;
                }
            }
        }
        FindObjectOfType<PlayerController>().GetComponent<PlayerController>().enabled = true;
    }

    public GameObject CheckChildren(Vector3 posToCheck)
    {
        // Recorremos los hijos y comprobamos si hay alguno en la posicion.
        // Se recorren por separado para que la deteccion de hijos tenga la siguiente prioridad: Wall > Block > Target.
        foreach (Transform child in WallParent.transform)
        {
            if (child.position == posToCheck)
            {
                return child.gameObject;
            }
        }
        foreach (Transform child in BlockParent.transform)
        {
            if (child.position == posToCheck)
            {
                return child.gameObject;
            }
        }
        foreach (Transform child in TargetParent.transform)
        {
            if (child.position == posToCheck)
            {
                return child.gameObject;
            }
        }
        return null;
    }

    public bool PushBlock(GameObject childToPush, Vector3 positionToPush)
    {
        // Empujar un bloque.
        // Se comprueba si en el lugar al que se va a empujar el bloque hay algo. En caso negativo (o que ese algo sea un target) se mueve el bloque.
        GameObject possibleObstacle = CheckChildren(positionToPush);
        if(possibleObstacle == null || possibleObstacle.tag == "Target")
        {
            childToPush.transform.position = positionToPush;
            return true;
        }
        return false;
    }

    public void Reset()
    {
        // Reiniciar el nivel.
        // Se destruyen todos los hijos, incluido el jugador. Despues se vuelve a dibujar el mapa actual.
        foreach (Transform child in WallParent.transform)
        {
            Destroy(child.gameObject);
        }
        foreach (Transform child in BlockParent.transform)
        {
            Destroy(child.gameObject);
        }
        foreach (Transform child in TargetParent.transform)
        {
            Destroy(child.gameObject);
        }

        Destroy(FindObjectOfType<PlayerController>().gameObject);

        DrawCurrentMap();
    }

    bool IsBlockInPosition(Vector3 position)
    {
        // Comprueba si hay un bloque en la posicion.
        foreach (Transform child in BlockParent.transform)
        {
            if(child.position == position)
            {
                return true;
            }
        }
        return false;
    }

    public void CheckWinCondition()
    {
        // Comprobar la condicion de victoria, que todos los target tengan un bloque encima.
        foreach(Transform child in TargetParent.transform)
        {
            if (!IsBlockInPosition(child.transform.position)){
                return;         // Si hay algun target que no tenga "pareja", no se cumple la condicion de viuctoria, por lo que retornamos.
            }
        }
        // Si todos los targets tienen un bloque sobre ellos:
        mapIndex++;     // Incrementamos el indice del mapa actual.
        if(mapIndex >= mapList.Length)
        {
            mapIndex = 0;       // Nos aseguramos que el indice no supere el numero de mapas.
        }
        Reset();        // Reiniciamos el nivel, lo que hace que se muestre el nuevo nivel.
    }

}
