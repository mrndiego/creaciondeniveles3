using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapCreationButtons : MonoBehaviour
{
    LevelEditorManager editor;
    public GameObject SaveMapScreen;
    public GameObject canvas;

    void Start()
    {
        editor = GameObject.FindGameObjectWithTag("LevelEditorManager").GetComponent<LevelEditorManager>();
    }

    public void ResetMap()
    {
        editor.ClearMap();
    }

    public void SaveMap()
    {
        //editor.SaveMap();
        Instantiate(SaveMapScreen, canvas.transform);
    }
    
}
