using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class LevelEditorManager : MonoBehaviour
{
    public int currentButtonPressed;
    public ItemController[] itemButtons;
    public GameObject[] itemPrefabs;
    public GameObject[] ItemImage;

    private void Update()
    {
        Vector2 screenPosition = new(Input.mousePosition.x, Input.mousePosition.y);
        Vector2 worldPosition = Camera.main.ScreenToWorldPoint(screenPosition);

        if (Input.GetMouseButtonDown(0) && itemButtons[currentButtonPressed].Clicked)
        {            
            itemButtons[currentButtonPressed].Clicked = false;

            // Solo se permite un Player por nivel.
            if (currentButtonPressed == 3)
            {
                GameObject temp = GameObject.FindGameObjectWithTag("Player");
                if(temp != null)
                {
                    Destroy(temp);
                }
            }
            Instantiate(itemPrefabs[currentButtonPressed], new Vector3((int)worldPosition.x, (int)worldPosition.y, 0), Quaternion.identity, transform);

            Destroy(GameObject.FindGameObjectWithTag("ItemImage"));
        }
    }

    public void ClearMap()
    {
        foreach (Transform child in transform)
        {
            Destroy(child.gameObject);
        }
    }

    public void SaveMap(string mapName)
    {
        int[,] map;

        int maxX = int.MinValue;
        int maxY = int.MinValue;
        int minX = int.MaxValue;
        int minY = int.MaxValue;

        int offsetX = 0;
        int offsetY = 0;
        int totalX, totalY;

        foreach (Transform child in transform)
        {
            if (child.transform.position.x > maxX)
            {
                maxX = (int)child.transform.position.x;
            }
            if (child.transform.position.y > maxY)
            {
                maxY = (int)child.transform.position.y;
            }
            if (child.transform.position.x < minX)
            {
                minX = (int)child.transform.position.x;
            }
            if (child.transform.position.y < minY)
            {
                minY = (int)child.transform.position.y;
            }
        }

        if (minX > 0)
        {
            totalX = maxX;
        }
        else
        {
            offsetX = Mathf.Abs(minX);
            totalX = maxX + offsetX;
        }
        if (minY > 0)
        {
            totalY = maxY;
        }
        else
        {
            offsetY = Mathf.Abs(minY);
            totalY = maxY + offsetY;
        }

        totalX++;
        totalY++;

        map = new int[totalX, totalY];
        foreach (Transform child in transform)
        {
            int posX = (int) child.transform.position.x + offsetX;
            int posY = totalY - (int) child.transform.position.y - offsetY-1;
            //Debug.Log(child.name+" posX: "+posX + " posY: " + posY+" totalX: "+totalX+" totalY: "+totalY+" OffsetX: "+offsetX+" OffsetY: "+offsetY);
            switch (child.tag)
            {
                case "Wall":
                    map[posX, posY] = 1;
                    break;
                case "Block":
                    map[posX, posY] = 2;
                    break;
                case "Target":
                    map[posX, posY] = 3;
                    break;
                case "Player":
                    map[posX, posY] = 4;
                    break;
            }
        }

        if (mapName == "") mapName = "DefaultMapName";
        Map newMap = new(Map.From2DTo1D(map, totalX, totalY), totalX, totalY, mapName);

        string jsonText = JsonUtility.ToJson(newMap);
        File.WriteAllText(Application.persistentDataPath + "/"+newMap.mapName+".json",jsonText);
    }
}
