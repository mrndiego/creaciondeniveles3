using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    GameManager manager;

    void Start()
    {
        manager = FindObjectOfType<GameManager>();
    }

    void Update()
    {
        if (Input.GetKeyUp(KeyCode.W) || Input.GetKeyUp(KeyCode.UpArrow))
        {
            Move(Vector3.up);
        }
        else if (Input.GetKeyUp(KeyCode.A) || Input.GetKeyUp(KeyCode.LeftArrow))
        {
            Move(Vector3.left);
        }
        else if (Input.GetKeyUp(KeyCode.S) || Input.GetKeyUp(KeyCode.DownArrow))
        {
            Move(Vector3.down);
        }
        else if (Input.GetKeyUp(KeyCode.D) || Input.GetKeyUp(KeyCode.RightArrow))
        {
            Move(Vector3.right);
        }
        else if (Input.GetKeyUp(KeyCode.R))
        {
            manager.Reset();
        }
    }

    void Move(Vector3 direction){

        GameObject posibleObstacle = manager.CheckChildren(transform.position + direction);
        if(posibleObstacle != null)
        {
            switch (posibleObstacle.tag)
            {
                case "Wall":
                    //Debug.Log("No te puedes mover a traves de las paredes");
                    return;
                case "Target":
                    //Debug.Log("Los targets se pueden atravesar sin problemas.");
                    break;
                case "Block":
                    //Debug.Log("Esto es un bloque, luego se movera");
                    if(!manager.PushBlock(posibleObstacle, transform.position + direction * 2))
                    {
                        //Debug.Log("No se puede mover este bloque.");
                        return;
                    }
                    break;
            }
        }
        transform.position += direction;
        manager.CheckWinCondition();
    }
}
