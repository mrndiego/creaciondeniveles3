using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SaveScreenButtons : MonoBehaviour
{
    LevelEditorManager editor;
    InputField inputField;

    public GameObject savedPanel;

    private void Start()
    {
        editor = GameObject.FindGameObjectWithTag("LevelEditorManager").GetComponent<LevelEditorManager>();
        inputField = FindObjectOfType<InputField>();
    }

    public void Cancel()
    {
        Destroy(gameObject);
    }
    public void Save()
    {
        editor.SaveMap(inputField.text);
        savedPanel.SetActive(true);
    }
}
